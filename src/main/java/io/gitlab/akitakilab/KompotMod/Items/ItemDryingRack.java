package io.gitlab.akitakilab.KompotMod.Items;

import com.bioxx.tfc.Core.TFCTabs;
import com.bioxx.tfc.Items.ItemBlocks.ItemTerraBlock;
import com.bioxx.tfc.api.Enums.EnumSize;
import net.minecraft.block.Block;
import net.minecraft.item.ItemStack;


/**
 * The item class of the glass can for making kompot.
 */
public class ItemDryingRack extends ItemTerraBlock {
	/**
	 * Constructor
	 * @param block
	 */
	public ItemDryingRack(Block block) {
		super(block);
		this.setCreativeTab(TFCTabs.TFC_DEVICES);
	}

	@Override
	public EnumSize getSize(ItemStack is) {
		return EnumSize.MEDIUM;
	}
}
